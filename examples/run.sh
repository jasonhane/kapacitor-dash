#!/bin/bash

FILE=$1

if [ -z "$FILE" ]; then
    echo "Usage: $0 FILE"
    echo ""
    echo "Example: $0 critical.json"
    exit 1
fi

DIR=$(dirname $0)

TARGET=$FILE
if [ ! -e "${FILE}" ]; then
    if [ -e "${DIR}/${FILE}" ]; then
        TARGET="${DIR}/${FILE}"
    else
        echo "ERROR: Could not find ${FILE} or ${DIR}/${FILE}"
        exit 1
    fi
fi

curl -vv -X POST -H 'Content-Type: application/json' http://localhost:5000/api/alert --data-binary "@${DIR}/${FILE}"

