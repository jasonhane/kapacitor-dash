# syntax=docker/dockerfile:1

ARG PYTHON_VERSION=3.12

# Stage 1: Build Stage
FROM python:${PYTHON_VERSION}-slim AS build

# Install necessary build tools and MariaDB client libraries
RUN apt-get update && \
    apt-get install -y build-essential libmariadb-dev-compat libmariadb-dev gcc pkg-config && \
    rm -rf /var/lib/apt/lists/*

# Create a virtual environment
RUN python -m venv /opt/venv

# Activate virtual environment and install dependencies
ENV PATH="/opt/venv/bin:$PATH"
COPY requirements.txt .
RUN pip install --upgrade pip && pip install -r requirements.txt

# Stage 2: Final Stage
FROM python:${PYTHON_VERSION}-slim

# Prevents Python from writing pyc files.
ENV PYTHONDONTWRITEBYTECODE=1

# Keeps Python from buffering stdout and stderr to avoid situations where
# the application crashes without emitting any logs due to buffering.
ENV PYTHONUNBUFFERED=1

# Create a non-privileged user that the app will run under.
ARG UID=10001
ARG USER=kapdash
RUN adduser \
    --disabled-password \
    --gecos "" \
    --home "/app" \
    --shell "/sbin/nologin" \
    --no-create-home \
    --uid "${UID}" \
    ${USER}

# Install runtime dependencies and MariaDB client libraries
RUN apt-get update && \
    apt-get install -y libmariadb3 && \
    rm -rf /var/lib/apt/lists/*

# Copy virtual environment from the build stage
COPY --from=build /opt/venv /opt/venv

# Activate virtual environment
ENV PATH="/opt/venv/bin:$PATH"

# Set the working directory
WORKDIR /app

# Copy the application code
ARG CACHEBUST
COPY . .

RUN chown -R ${USER}:${USER} /app
USER ${USER}

# Expose the port (if your application runs on a specific port)
EXPOSE 5000

# Command to run the application
CMD ["python3", "main.py"]

