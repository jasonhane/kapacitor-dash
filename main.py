import uvicorn

from app import create_app
from config import DevelopmentConfig, ProductionConfig, config

# Check for environment variable for development mode
env_mode = config.environment
config_class = DevelopmentConfig() if env_mode == 'development' else ProductionConfig()

# Create the app with the appropriate configuration
app = create_app()

if __name__ == "__main__":
    server_config = config.server

    print(f"Running in environment {env_mode}")

    uvicorn.run("main:app",
                host=server_config["bind_address"],
                port=server_config["listen_port"],
                log_level=server_config["log_level"],
                reload=(env_mode == 'development'),
                workers=server_config["uvicorn_workers"]
                )
