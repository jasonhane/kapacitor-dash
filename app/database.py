import time

from sqlalchemy import create_engine
from sqlalchemy.exc import OperationalError
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker

from config import config

DATABASE_URL = ""

db_config = config.database

if db_config['type'] == 'sqlite':
    DATABASE_URL = f"sqlite:///{db_config['name']}"
elif db_config['type'] == 'mysql':
    DATABASE_URL = f"mysql+pymysql://{db_config['user']}:{db_config['password']}@{db_config['host']}:{db_config['port']}/{db_config['dbname']}"

engine = create_engine(DATABASE_URL)
SessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=engine)
Base = declarative_base()


def init_db():
    retries = 5
    delay = 1

    for i in range(retries):
        try:
            # Try to connect to the database
            connection = engine.connect()
            connection.close()
            return
        except OperationalError as e:
            print(f"Database connection failed: {e.orig}")
            print(f"Retrying in {delay} seconds...")
            time.sleep(delay)
            delay *= 2  # Exponential backoff

    print(f"Failed to connect to the database after {retries} retries. Exiting.")
    raise Exception("Failed to connect to the database.")


# Dependency
def get_db():
    db = SessionLocal()
    try:
        yield db
    finally:
        db.close()
