from datetime import datetime


def pretty_print_time(minutes):
    days = minutes // 1440
    remaining_minutes = minutes % 1440
    hours = remaining_minutes // 60
    minutes = remaining_minutes % 60

    return f"{days}d, {hours}h, {minutes}m"


def parse_alert_time(time_str):
    return datetime.strptime(time_str, '%Y-%m-%dT%H:%M:%SZ')
