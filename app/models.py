from datetime import datetime

from sqlalchemy import Column, String, DateTime, Boolean, Index, UniqueConstraint
from sqlalchemy.dialects.mysql import INTEGER

from app.database import Base


# The alert.id and history.alert_id is the unique event ID for that alert

class Alert(Base):
    __tablename__ = "alerts"
    id = Column(INTEGER(unsigned=True), primary_key=True, autoincrement=True)
    kapacitor_id = Column(String(200), nullable=False)
    message = Column(String(200), nullable=False)
    duration = Column(INTEGER(unsigned=True), nullable=False)
    severity = Column(String(20), nullable=False)
    host = Column(String(50), nullable=False)
    time = Column(DateTime, nullable=False)

    __table_args__ = (
        UniqueConstraint('kapacitor_id', name='uq_kapacitor_id'),
        Index('ix_severity', 'severity'),
        Index('ix_host', 'host'),
        Index('ix_time', 'time'),
    )

    __mapper_args__ = {
        "eager_defaults": True
    }


class History(Base):
    __tablename__ = "history"
    id = Column(INTEGER(unsigned=True), primary_key=True, index=True)
    alert_id = Column(INTEGER(unsigned=True), index=True)
    kapacitor_id = Column(String(200), nullable=False, index=True)
    message = Column(String(200), nullable=False)
    duration = Column(INTEGER(unsigned=True), nullable=False)
    severity = Column(String(20), nullable=False, index=True)
    host = Column(String(50), nullable=False, index=True)
    time = Column(DateTime, nullable=False)
    closed = Column(Boolean(), default=False, nullable=False, index=True)
    manually_closed = Column(Boolean(), default=False, nullable=False, index=True)
    inserted_at = Column(DateTime, default=datetime.utcnow, nullable=False)
    updated_at = Column(DateTime, default=datetime.utcnow, nullable=False, onupdate=datetime.utcnow)

    __table_args__ = (
        UniqueConstraint('alert_id', 'severity', name='uq_event'),
        Index('ix_alert_id', 'alert_id'),
        Index('ix_severity', 'severity'),
        Index('ix_host', 'host'),
        Index('ix_time', 'time'),
    )
