from fastapi import APIRouter, Depends
from sqlalchemy.orm import Session

from app.database import get_db

admin_router = APIRouter()


@admin_router.post("/reinitialize")
async def reinitialize(db: Session = Depends(get_db)):
    from app.models import Base
    Base.metadata.drop_all(bind=db.bind)
    Base.metadata.create_all(bind=db.bind)
    return {"status": "success"}
