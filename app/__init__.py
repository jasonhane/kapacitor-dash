from pathlib import Path

from fastapi import FastAPI, Request
from fastapi.exceptions import RequestValidationError
from fastapi.responses import JSONResponse
from fastapi.staticfiles import StaticFiles

from app.admin import admin_router
from app.alerts import alerts_router
from app.database import engine, Base, SessionLocal, init_db
from app.routes import main_router

app = FastAPI()


def create_app():
    app.include_router(main_router, prefix="")
    app.include_router(alerts_router, prefix="/api")
    app.include_router(admin_router, prefix="/admin")

    # Set up the static files directory
    current_dir = Path(__file__).resolve().parent
    static_dir = current_dir.parent / "static"
    app.mount("/static", StaticFiles(directory=static_dir), name="static")

    # Set up the database
    init_db()
    SessionLocal.configure(bind=engine)
    Base.metadata.create_all(bind=engine)

    return app


@app.exception_handler(RequestValidationError)
async def validation_exception_handler(request: Request, exc: RequestValidationError):
    return JSONResponse(
        status_code=422,
        content={"detail": exc.errors()}
    )
