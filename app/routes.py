import asyncio

from fastapi import APIRouter, Depends, Request
from fastapi.responses import StreamingResponse, RedirectResponse
from fastapi.templating import Jinja2Templates
from sqlalchemy.orm import Session

from app.database import get_db
from app.models import Alert

main_router = APIRouter()
templates = Jinja2Templates(directory="templates")

clients = []


# Add redirect from / to /dashboard
@main_router.get("/", include_in_schema=False)
async def root():
    return RedirectResponse(url="/dashboard")


@main_router.get("/dashboard")
async def dashboard(request: Request, db: Session = Depends(get_db)):
    alerts = db.query(Alert).all()
    return templates.TemplateResponse("dashboard.html", {"request": request, "alerts": alerts})


@main_router.get("/history")
async def history(request: Request,
                  start_date: str = None,
                  end_date: str = None,
                  search: str = None,
                  limit: str = "100",
                  page: str = "1"):
    return templates.TemplateResponse("history.html", {
        "request": request,
        "start_date": start_date,
        "end_date": end_date,
        "search": search,
        "limit": limit,
        "page": page
    })


@main_router.get("/events")
async def events():
    async def event_generator():
        q = asyncio.Queue()
        clients.append(q)
        try:
            while True:
                await q.get()
                yield 'data: refresh\n\n'
        except asyncio.CancelledError:
            clients.remove(q)

    return StreamingResponse(event_generator(), media_type="text/event-stream")


def notify_clients():
    for client in clients:
        try:
            client.put_nowait(None)
        except:
            pass
