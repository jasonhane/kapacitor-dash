from datetime import datetime
from typing import List, Optional, Dict, Any

from fastapi import APIRouter, HTTPException, Depends, Request, Query
from pydantic import BaseModel
from sqlalchemy import update
from sqlalchemy.dialects.mysql import insert
from sqlalchemy.orm import Session

from app.database import get_db
from app.models import Alert, History
from app.routes import notify_clients
from app.utils import parse_alert_time, pretty_print_time

alerts_router = APIRouter()


class Series(BaseModel):
    name: str
    tags: Dict[str, str]
    columns: List[str]
    values: List[List[Any]]


class Data(BaseModel):
    series: List[Series]


class AlertBase(BaseModel):
    id: str
    message: str
    details: str
    time: str
    duration: int
    level: str
    data: Data
    previousLevel: Optional[str]
    recoverable: Optional[bool]


@alerts_router.post("/alert")
async def create_alert(alert: AlertBase, db: Session = Depends(get_db)):
    alert_time = parse_alert_time(alert.time)

    try:
        host = alert.data.series[0].tags['host']
    except KeyError:
        host = None

    duration = alert.duration / 60000000000 if alert.duration > 0 else 0

    alert_data = {
        "kapacitor_id": alert.id,
        "message": alert.message,
        "duration": duration,
        "severity": alert.level,
        "host": host,
        "time": alert_time,
    }

    history_data = alert_data.copy()

    try:
        if alert.level != 'OK':
            # Update alerts table
            stmt = insert(Alert).values(alert_data).on_duplicate_key_update(
                #                id=alert_data['id'],
                message=alert_data['message'],
                duration=alert_data['duration'],
                severity=alert_data['severity'],
                #                host=alert_data['host'],
                #                time=alert_data['time']
            )

            result = db.execute(stmt)
            db.commit()

            # Update history table.  If the event already exists and everything else is the same,
            # only update the duration. Changing of severity is a new history event.
            history_data['alert_id'] = result.inserted_primary_key[0]
            stmt = insert(History).values(history_data).on_duplicate_key_update(
                duration=alert_data['duration'],
            )

            db.execute(stmt)
            db.commit()
        else:
            # Get the alert to remove
            db_alert = db.query(Alert).filter(Alert.kapacitor_id == alert.id).first()
            if db_alert:
                db.delete(db_alert)
                db.commit()

                # Update the history database and set closed to True for that alert ID
                db.execute(update(History).where(History.alert_id == db_alert.id).values(closed=True))

                # Insert the recovery into the history table
                history_data['alert_id'] = db_alert.id
                history_data['closed'] = True
                stmt = insert(History).values(history_data)

                db.execute(stmt)
                db.commit()

        notify_clients()

        return {"status": "success"}

    except Exception as e:
        db.rollback()
        raise HTTPException(status_code=400, detail=str(e))


@alerts_router.delete("/alert/{alert_id}")
async def delete_alert(alert_id: str, db: Session = Depends(get_db)):
    alert = db.query(Alert).filter(Alert.kapacitor_id == alert_id).first()
    if alert:
        db.delete(alert)
        db.commit()

        history_data = {
            "alert_id": alert.id,
            "kapacitor_id": alert.kapacitor_id,
            "message": alert.message,
            "duration": alert.duration,
            "severity": "OK",
            "host": alert.host,
            "time": alert.time,
            "closed": True,
            "manually_closed": True,
        }

        # Update the history table and set the alert ID to closed
        db.execute(update(History).where(History.alert_id == alert.id).values(closed=True, manually_closed=True))

        # Insert the recovery into the history table
        db.execute(insert(History).values(history_data))
        db.commit()

    else:
        raise HTTPException(status_code=404, detail="Alert not found")
    return {"status": "success"}


@alerts_router.delete("/alerts")
async def delete_all_alerts(db: Session = Depends(get_db)):
    db.query(Alert).delete()
    db.commit()
    return {"status": "success"}


@alerts_router.get("/alerts")
async def list_alerts(
        request: Request,
        check: Optional[str] = Query(None),
        duration: Optional[int] = Query(None),
        severity: Optional[str] = Query(None),
        host: Optional[str] = Query(None),
        message: Optional[str] = Query(None),
        db: Session = Depends(get_db)

):
    query = db.query(Alert)

    if check:
        query = query.filter(Alert.kapacitor_id.like(f"{check}-%"))
    if duration:
        query = query.filter(Alert.duration >= duration)
    if severity:
        query = query.filter(Alert.severity == severity.upper())
    if host:
        query = query.filter(Alert.host == host)
    if message:
        query = query.filter(Alert.message.like(f"%{message}%"))

    alerts = query.all()

    alert_list = [{
        "time": alert.time,
        "id": alert.kapacitor_id,
        "check": alert.kapacitor_id.split('-')[0],
        "duration": pretty_print_time(alert.duration),
        "message": alert.message,
        "severity": alert.severity,
        "host": alert.host
    } for alert in alerts]

    return {'data': alert_list}


@alerts_router.get("/history")
async def history(
        start_date: str = None,
        end_date: str = None,
        search: str = None,
        limit: str = "100",
        page: str = "1",
        db: Session = Depends(get_db)):
    try:
        limit = int(limit)
        page = int(page)
    except ValueError:
        limit = 100
        page = 1

    query = db.query(History)

    if start_date:
        query = query.filter(History.time >= datetime.strptime(start_date, '%Y-%m-%d %H:%M:%S'))
    if end_date:
        query = query.filter(History.time <= datetime.strptime(end_date, '%Y-%m-%d %H:%M:%S'))
    if search:
        search = f"%{search}%"
        query = query.filter((History.message.ilike(search)) | (History.severity.ilike(search)))

    total_records = query.count()
    records = query.order_by(History.inserted_at.desc()).offset((page - 1) * limit).limit(limit).all()

    history_list = [{
        'alert_id': record.alert_id,
        'kapacitor_id': record.kapacitor_id,
        'check': record.kapacitor_id.split('-')[0],
        'message': record.message,
        'duration': pretty_print_time(record.duration),
        'severity': record.severity,
        'host': record.host,
        'time': record.time.strftime('%Y-%m-%d %H:%M:%S'),
        'inserted_at': record.inserted_at.strftime('%Y-%m-%d %H:%M:%S'),
        'updated_at': record.updated_at.strftime('%Y-%m-%d %H:%M:%S'),
        'closed': record.closed,
        'manually_closed': record.manually_closed,
        'group': record.time.strftime('%Y-%m-%d %H:00:00')
    } for record in records]

    return {
        'data': history_list,
        'total_records': total_records,
        'page': page,
        'limit': limit
    }
