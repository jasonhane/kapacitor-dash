import os

import yaml


class Config:
    def __init__(self, config_file='config.yaml'):
        self.config_file = config_file
        self.config_data = self.load_config()
        # Load environment from env variable or config file
        self.env_mode = os.getenv('ENV_MODE', self.config_data.get('environment', 'production'))

    def load_config(self):
        with open(self.config_file, 'r') as file:
            return yaml.safe_load(file)

    @property
    def database(self):
        return self.config_data.get(self.env_mode, {}).get('database', {})

    @property
    def server(self):
        server_config = self.config_data.get(self.env_mode, {}).get('server', {})
        return {
            "bind_address": os.getenv('BIND_ADDRESS', server_config.get('bind_address', '0.0.0.0')),
            "listen_port": int(os.getenv('LISTEN_PORT', server_config.get('listen_port', 5000))),
            "log_level": os.getenv('LOG_LEVEL', server_config.get('log_level', 'info')),
            "uvicorn_workers": int(os.getenv('UVICORN_WORKERS', server_config.get('uvicorn_workers', 4)))
        }

    @property
    def environment(self):
        return self.env_mode


# Configuration classes
class DevelopmentConfig(Config):
    def __init__(self):
        super().__init__()
        self.DEBUG = True


class ProductionConfig(Config):
    def __init__(self):
        super().__init__()
        self.DEBUG = False


# Load the configuration
config = Config()
